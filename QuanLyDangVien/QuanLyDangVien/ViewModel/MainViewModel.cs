﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace QuanLyDangVien.ViewModel
{
    // data context cho mainwindow
    public class MainViewModel : BaseViewModel
    {
        public bool Isloaded = false;
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand NationCommand { get; set; }
        public ICommand ComputerSkillCommand { get; set; }
        public ICommand PartyPositionCommand { get; set; }
        public ICommand GovernmentPositionCommand { get; set; }
        public ICommand AssociationPositionCommand { get; set; }
        public ICommand ObjectCommand { get; set; }
        public ICommand SpecializeCommand { get; set; }
        public ICommand PoliticalTheoryCommand { get; set; }
        public ICommand ReligionCommand { get; set; }
        public ICommand ForeignLanguageCommand { get; set; }
        public ICommand BonusCommand { get; set; }
        public ICommand SpecializedCommand { get; set; }
        public ICommand DisciplineCommand { get; set; }
        public ICommand ClassifyCommand { get; set; }
        public ICommand BranchCommand { get; set; }
        public ICommand CompanyChageCommand { get; set; }
        public ICommand UserCommand { get; set; }
        public ICommand AccountCommand { get; set; }
        public ICommand ProfileCommand { get; set; }
        public ICommand StatusCommand { get; set; }
        public ICommand PartyCommitteeCommand { get; set; }
        // mọi thứ xử lý sẽ nằm trong này
        public MainViewModel()
        {
            LoadedWindowCommand = new RelayCommand<object>((p) => { return true; }, (p) => {
                Isloaded = true;
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.ShowDialog();
            }
              );

            NationCommand = new RelayCommand<object>((p) => { return true; }, (p) => { NationWindow wd = new NationWindow(); wd.ShowDialog();});
            ComputerSkillCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ComputerSkillWindow wd = new ComputerSkillWindow(); wd.ShowDialog(); });
            PartyPositionCommand = new RelayCommand<object>((p) => { return true; }, (p) => { PartyPositionWindow wd = new PartyPositionWindow(); wd.ShowDialog(); });
            GovernmentPositionCommand = new RelayCommand<object>((p) => { return true; }, (p) => { GovernmentPositionWindow wd = new GovernmentPositionWindow(); wd.ShowDialog(); });
            AssociationPositionCommand = new RelayCommand<object>((p) => { return true; }, (p) => { AssociationPositionWindow wd = new AssociationPositionWindow(); wd.ShowDialog(); });
            ObjectCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ObjectWindow wd = new ObjectWindow(); wd.ShowDialog(); });
            SpecializeCommand = new RelayCommand<object>((p) => { return true; }, (p) => { SpecializeWindow wd = new SpecializeWindow(); wd.ShowDialog(); });
            PoliticalTheoryCommand = new RelayCommand<object>((p) => { return true; }, (p) => { PoliticalTheoryWindow wd = new PoliticalTheoryWindow(); wd.ShowDialog(); });
            ReligionCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ReligionWindow wd = new ReligionWindow(); wd.ShowDialog(); });
            ForeignLanguageCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ForeignLanguageWindow wd = new ForeignLanguageWindow(); wd.ShowDialog(); });
            SpecializedCommand = new RelayCommand<object>((p) => { return true; }, (p) => { SpecializedWindow wd = new SpecializedWindow(); wd.ShowDialog(); });
            BonusCommand = new RelayCommand<object>((p) => { return true; }, (p) => { BonusWindow wd = new BonusWindow(); wd.ShowDialog(); });
            DisciplineCommand = new RelayCommand<object>((p) => { return true; }, (p) => { DisciplineWindow wd = new DisciplineWindow(); wd.ShowDialog(); });
            ClassifyCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ClassifyWindow wd = new ClassifyWindow(); wd.ShowDialog(); });
            BranchCommand = new RelayCommand<object>((p) => { return true; }, (p) => { BranchWindow wd = new BranchWindow(); wd.ShowDialog(); });
            CompanyChageCommand = new RelayCommand<object>((p) => { return true; }, (p) => { CompanyChageWindow wd = new CompanyChageWindow(); wd.ShowDialog(); });
            UserCommand = new RelayCommand<object>((p) => { return true; }, (p) => { UserWindow wd = new UserWindow(); wd.ShowDialog(); });
            AccountCommand = new RelayCommand<object>((p) => { return true; }, (p) => { AccountWindow wd = new AccountWindow(); wd.ShowDialog(); });
            ProfileCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ProfileWindow wd = new ProfileWindow(); wd.ShowDialog(); });
            StatusCommand = new RelayCommand<object>((p) => { return true; }, (p) => { StatusWindow wd = new StatusWindow(); wd.ShowDialog(); });
            PartyCommitteeCommand = new RelayCommand<object>((p) => { return true; }, (p) => { PartyCommitteeWindow wd = new PartyCommitteeWindow(); wd.ShowDialog(); });

        }
    }
}
